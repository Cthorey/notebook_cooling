Notebook on the intrusion cooling
================

Presentation material for my work on the cooling of a magmatic intrusion

# Presentation Index

* Calcul formell
  - [Calcul_Formelle.ipynb](http://nbviewer.ipython.org/github/cthorey/Notebook_Cooling/blob/master/Calcul_Formell.ipynb)

* Bending regime

    - Isothermal & Isoviscous Case
        - [I_TV_1.ipynb](http://nbviewer.ipython.org/github/cthorey/Notebook_Cooling/blob/master/I_TV_1.ipynb)
        
    - Isoviscous Case
      - [I_V_1.ipynb](http://nbviewer.ipython.org/github/cthorey/Notebook_Cooling/blob/master/I_V_1.ipynb)
      
    - Temperaturde Dependant Viscosity Case
      - [TDV_1.ipynb](http://nbviewer.ipython.org/github/cthorey/Notebook_Cooling/blob/master/TDV_1.ipynb)
      - [TDV_2.ipynb](http://nbviewer.ipython.org/github/cthorey/Notebook_Cooling/blob/master/TDV_2.ipynb)
      - [TDV_3.ipynb](http://nbviewer.ipython.org/github/cthorey/Notebook_Cooling/blob/master/TDV_3.ipynb)
      - [TDV_4.ipynb](http://nbviewer.ipython.org/github/cthorey/Notebook_Cooling/blob/master/TDV_4.ipynb) 
      - [TDV_5.ipynb](http://nbviewer.ipython.org/github/cthorey/Notebook_Cooling/blob/master/TDV_5.ipynb)

* Gravity current regime
